package com.geoquiz.view.menu;

import com.geoquiz.view.utility.Background;

import java.awt.*;
import java.io.IOException;

import com.geoquiz.view.button.Buttons;
import com.geoquiz.view.button.ButtonsCategory;
import com.geoquiz.view.button.MyButton;
import com.geoquiz.view.button.MyButtonFactory;
import com.geoquiz.view.label.MyLabel;
import com.geoquiz.view.label.MyLabelFactory;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * The scene where user can choose game category.
 */
public class CategoryScene extends Scene {

    private static final double POS_1_X = 100;
    private static final double POS_1_Y = 450;
    private static final double POS_2_X = 250;
    private static final double POS_2_Y = 300;
    private static final double POS_X_BACK = 450;
    private static final double POS_Y_BACK = 600;
    private static final double BUTTON_WIDTH = 350;
    private static final Text BUTTON_PRESSED = new Text();
    private static final double USER_LABEL_FONT = 40;

    private final Pane panel = new Pane();

    private final HBox hbox = new HBox(10);
    private final HBox hbox2 = new HBox(10);
    private final VBox vbox = new VBox();

    /**
     * @param mainStage
     *            the stage where the scene is called.
     */
    public CategoryScene(final Stage mainStage) {
        super(new StackPane(), mainStage.getWidth(), mainStage.getHeight());

        final MyButton capitals = createButton(ButtonsCategory.CAPITALI.toString());
        final MyButton currencies = createButton(ButtonsCategory.VALUTE.toString());
        final MyButton dishes = createButton(ButtonsCategory.CUCINA.toString());
        final MyButton monuments = createButton(ButtonsCategory.MONUMENTI.toString());
        final MyButton flags = createButton(ButtonsCategory.BANDIERE.toString());
        final MyButton back = createButton(Buttons.INDIETRO.toString());

        final MyLabel userLabel = MyLabelFactory.createMyLabel("USER: " + LoginMenuScene.getUsername(), Color.BLACK,
                USER_LABEL_FONT);

        hbox.setTranslateX(POS_1_X);
        hbox.setTranslateY(POS_1_Y);

        hbox2.setTranslateX(POS_2_X);
        hbox2.setTranslateY(POS_2_Y);

        vbox.setTranslateX(POS_X_BACK);
        vbox.setTranslateY(POS_Y_BACK);

        hbox.getChildren().addAll((Node) flags, (Node) currencies, (Node) dishes);
        hbox2.getChildren().addAll((Node) monuments, (Node) capitals);
        vbox.getChildren().add((Node) back);

        setOnMouseClicked(back, () -> {
            try {
                mainStage.setScene(new MainMenuScene(mainStage));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        changeSceneOnClick(capitals, mainStage);
        changeSceneOnClick(monuments, mainStage);
        changeSceneOnClick(currencies, mainStage);
        changeSceneOnClick(flags, mainStage);
        changeSceneOnClick(dishes, mainStage);

        this.panel.getChildren().addAll(Background.getImage(), Background.createBackground(), hbox, hbox2, vbox,
                Background.getLogo(), (Node) userLabel);

        this.setRoot(this.panel);
    }

    private MyButton createButton(final String name) {
        return MyButtonFactory.createMyButton(name, Color.BLUE, BUTTON_WIDTH);
    }

    private void changeSceneOnClick(final MyButton button, final Stage mainStage) {
        setOnMouseClicked(button, () -> {
            BUTTON_PRESSED.setText(button.getText());
            mainStage.setScene(new ModeScene(mainStage));
        });
    }

    private void setOnMouseClicked(final MyButton button, final Runnable handler) {
        final Node buttonNode = (Node) button;
        buttonNode.setOnMouseClicked(event -> {
            playClickSound();
            handler.run();
        });
    }

    private void playClickSound() {
        if (!MainWindow.isWavDisabled()) {
            MainWindow.playClick();
        }
    }

    /**
     * @return category.
     */
    public static String getCategoryPressed() {
        return BUTTON_PRESSED.getText();
    }
}
